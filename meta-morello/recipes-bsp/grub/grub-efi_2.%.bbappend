

GRUB_BUILDIN:morello-soc = " boot chain configfile ext2 fat gzio help http linux loadenv \
    lsefi normal net ntfs ntfscomp part_gpt part_msdos progress read search \
    search_fs_file search_fs_uuid search_label terminal terminfo tftp \
    "

GRUB_BUILDIN:morello-fvp = " boot chain configfile ext2 fat gzio help linux loadenv \
    lsefi normal ntfs ntfscomp part_gpt part_msdos progress read search \
    search_fs_file search_fs_uuid search_label terminal terminfo \
    "