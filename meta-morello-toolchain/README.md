meta-morello-toolchain
======================

This layer provides the Morello LLVM and GCC toolchains.
It also contains the C libraries.

Contributing
------------

We accept patches through the mailing list only.  

https://op-lists.linaro.org/mailman3/lists/linux-morello-distros.op-lists.linaro.org/

maintainer
----------
* Pawel Zalewski <pzalewski@thegoodpenguin.co.uk>